import User from '../models/User.model.js'
import _ from 'lodash'
import createToken from '../utils/createToken.js'
import path from 'path'

export const createUser = async (req, res, next) => {
    try {

        let { email, names, password } = req.body
        console.log(req.body)
        if (!email || !names || !password) return res.status(400).json({ status: false, message: 'email, names and password are all required' })
        if (!req.file) return res.status(400).json({ status: false, message: 'you need to upload your resume' })
        let resumeName = req.file?.filename

        const userExists = await User.findOne({ email })
        if (userExists) return res.status(409).json({ status: false, message: 'user already registered' })
        let newUser = new User({
            email,
            names,
            password,
            resume: resumeName
        })

        await newUser.save()
        const token = await createToken(newUser?._id)
        res.status(201).json({ status: true, user: _.pick(newUser, ['_id', 'email', 'names', 'resume']), access_token: token })
    } catch (err) {
        res.status(500).json({ status: false, message: err.message })
    }

}

export const getUserResume = async (req, res, next) => {
    try {

        let { userId } = req.params
        let userResume = await User.findById(userId).select('resume')
        if (!userResume) return res.status(404).json({ status: false, message: 'user was not found' })

        console.log(userResume);
        res.sendFile(path.join(process.cwd(), 'uploads', userResume.resume))
    } catch (err) {

        res.status(500).json({ status: false, message: err.message })

    }
}