import bcrypt from 'bcrypt'
import _ from 'lodash'
import User from '../models/User.model.js'
import createToken from '../utils/createToken.js'

export const Login = async (req, res, next) => {
    try {
        let { email, password } = req.body
        if (!email || !password) return res.status(400).json({ status: false, message: 'email and password are all required' })
        const userExists = await User.findOne({ email })
        if (!userExists) return res.status(409).json({ status: false, message: 'invalid email or password' })

        let pwd = await bcrypt.compare(password, userExists.password)
        if (!pwd) return res.status(404).json({ status: false, message: 'invalid password or email address' })

        const token = await createToken(userExists?._id)
        res.status(200).json({
            status: true,
            user: _.pick(userExists, ['_id', 'email', 'names', 'resume']),
            access_token: token
        })
    }
    catch (err) {
        res.status(500).json({ status: false, message: err.message })
    }
}

export const getMe = async (req, res, next) => {
    try {
        let { userId } = req
        let user = await User.findById(userId).select('-password')
        if (!user) return res.status(404).json({ status: false, message: 'user was not found' })
        res.status(200).json({
            status: true,
            user
        })
    } catch (err) {
        res.status(500).json({ status: false, message: err.message })
    }
}