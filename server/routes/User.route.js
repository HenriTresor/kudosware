import multer from 'multer'
import { createUser, getUserResume } from '../controllers/User.controller.js'

const upload = multer({ dest: './uploads' })

import { Router } from 'express'

const router = Router()

router.post('/', upload.single('resume'), createUser)

router.get('/resumes/:userId', getUserResume)
export default router