import { Login, getMe } from '../controllers/Auth.controller.js'
import verifyToken from '../middlewares/verifyToken.js'
import { Router } from 'express'


const router = Router()

router.post('/login', Login)
router.get('/me', verifyToken, getMe)
export default router