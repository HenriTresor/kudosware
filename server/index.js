import express from 'express'
import connection from './configs/db.config.js'
import userRouter from './routes/User.route.js'
import AuthRouter from './routes/Auth.route.js'
import cors from 'cors'

const app = express()
const PORT = process.env.PORT || 7070


connection.then(() => {
    console.log(`connecting to mongoose`);
}).then(() => {
    app.listen(PORT, () => {
        console.log(`SERVER IS LIVE ON ${PORT}`);
    })

}).catch((err) => console.error('error connecting to mongoose', err.message))

app.use(cors())
app.use(express.json())
app.use('/api/users', userRouter)
app.use('/api/auth', AuthRouter)

app.all("*", (req, res) => {
    res.status(404).json({ status: false, message: 'nothing on this route. Try another one!' })
})