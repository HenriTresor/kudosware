import jwt from 'jsonwebtoken'

export default async function verifyToken(req, res, next) {
    try {

        let token = req.headers['authorization'].split(" ")[1]
        if (!token) return res.status(403).json({ status: false, message: 'token is missing' })
        let decodedToken = await jwt.verify(token, 'my-secret-key')
        req.userId = decodedToken.id
        next()
    } catch (err) {
        res.status(401).json({ status: false, message: err.message })
    }
}