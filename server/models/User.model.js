import { hash } from 'bcrypt';
import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    email: { type: String, required: true, lowercase: true, trim: true, unique: true },
    names: { type: String, required: true, trim: true },
    password: { type: String, required: true, min: 6 },
    resume: { type: String, required: true }
})

userSchema.pre('save', async function () {
    try {

        let hashedPwd = await hash(this.password, 10)
        this.password = hashedPwd
    } catch (error) {
        console.log('error hashing password', error.message);
    }
})

const User = mongoose.model('users', userSchema)

export default User