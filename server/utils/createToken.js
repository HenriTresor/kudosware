import jwt from 'jsonwebtoken'

const createToken = async (id) => {
    return jwt.sign({ id }, 'my-secret-key')
}
export default createToken