import { Box, Typography } from '@mui/material'
import React from 'react'

const NotFound = () => {
  return (
      <Box
          sx={{
              display: 'grid',
              placeContent: 'center',
              textAlign: 'center',
              height:'100dvh'
      }}
      >
          
          <Typography
              variant='h3'
              sx={{
                  mb:3
              }}
          >
              Oops,
          </Typography>
          <Typography
          variant='h5'
          >
              This page was not found
          </Typography>
   </Box>
  )
}

export default NotFound