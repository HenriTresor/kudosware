import React from 'react'
import {
    Snackbar,
    Container,
    Button,
    Typography,
    Card, Box, TextField, InputLabel
} from '@mui/material'
import { rootLink } from '../utils/links.js'
import { Link } from 'react-router-dom'

const reducer = (state, action) => {
    switch (action.type) {
        case 'FETCHING':
            return { ...state, loading: true }
        case 'DONE_FETCHING':
            return { ...state, loading: false, data: action.payload }
        case 'ERROR':
            return {
                ...state, loading: false, error: action.payload
            }
        default:
            return { ...state, loading: false, error: null, data: null }
    }

}
const Signup = () => {

    const [inputValues, setInputValues] = React.useState({
        email: '',
        names: '',
        password: '',
    })

    const [{ loading, error, data }, dispatch] = React.useReducer(reducer, {
        loading: false,
        error: null,
        data: null
    })

    const [resume, setResume] = React.useState('')

    const handleChange = (e) => {
        setInputValues(prev => {
            return {
                ...prev,
                [e.target.name]: e.target.value
            }
        })
    }

    const handleSubmit = async () => {
        try {
            dispatch({ type: 'FETCHING' })
            const formData = new FormData()
            formData.append('email', inputValues.email)
            formData.append('names', inputValues.names)
            formData.append('password', inputValues.password)
            formData.append('resume', resume)
            const res = await fetch(`${rootLink}/api/users`, {
                method: 'POST',

                body: formData
            })

            const data = await res.json()

            dispatch({ type: 'DONE_FETCHING', payload: data.message })
            if (data.status) {
                localStorage.setItem('access_token', data.access_token)
                location.assign('/')
            }
        }
        catch (err) {
            console.log(err)
            dispatch({ type: 'ERROR', payload: err.message })
        }
    }

    return (
        <Container
            sx={{
                marginTop: '5em'
            }}
        >
            <Snackbar
                open={error || data}
                message={error || data}
                onClose={() => dispatch({ type: 'NONE' })}
            />
            <Card
                // variant=''
                elevation={7}
                sx={{
                    padding: 3,
                    display: 'flex',
                    flexDirection: 'column',
                    width: '65dvw'

                }}
            >
                <Box>

                    <Typography
                        variant='h5'
                    >
                        Create account
                    </Typography>
                    <Typography
                        sx={{ p: 3 }}
                        variant='body2'
                    >
                        YourHR is an online platform which helps job-seekers connect with different recruiters.
                    </Typography>
                </Box>
                <Box>
                    <Box
                        className='input-container'
                    >
                        <InputLabel htmlFor='email'>Input your email :</InputLabel>
                        <TextField
                            fullWidth
                            label='email'
                            id='email'
                            value={inputValues.email}
                            onChange={(e) => handleChange(e)}
                            name='email'
                        />
                    </Box>
                    <Box
                        className='input-container'
                    >
                        <InputLabel htmlFor='names'>Input your names :</InputLabel>
                        <TextField
                            fullWidth
                            label='names'
                            id='names'
                            value={inputValues.names}
                            onChange={(e) => handleChange(e)}
                            name='names'
                        />
                    </Box>
                    <Box
                        className='input-container'
                    >
                        <InputLabel htmlFor='password'>Create password</InputLabel>
                        <TextField
                            fullWidth
                            type='password'
                            label='password'
                            name='password'
                            value={inputValues.password}
                            onChange={(e) => handleChange(e)}
                            id='password'
                        />
                    </Box>
                    <Box
                        className='input-container'
                    >
                        <InputLabel htmlFor='resume'
                            style={{
                                cursor: 'pointer',
                                background: 'lightgrey',
                                padding: '0.5em',
                                textAlign: 'center'
                            }}
                        >{resume !== '' ? resume?.name?.slice(0, 35) : ' upload your resume'}</InputLabel>
                        <input type='file'
                            onChange={(e) => {
                                if (e.target.files[0].type === 'application/pdf') {
                                    setResume(e.target.files[0])
                                    return
                                }
                                dispatch({ type: 'ERROR', payload: 'only pdf formats are allowed' })
                            }}
                            name='resume' id='resume' style={{ display: 'none' }} />
                    </Box>
                    <Box
                        className='input-container'
                    >
                        <Button
                            variant='contained'
                            color='info'
                            onClick={handleSubmit}
                            disabled={loading}
                        >

                            Create account
                        </Button>
                    </Box>


                </Box>

                <Box className='input-container'>
                    <Typography>
                        Already have an account ?  <Link to='/login'>login</Link>
                    </Typography>
                </Box>
            </Card>
        </Container>
    )
}

export default Signup