import React from 'react'
import {
    Snackbar,
    Container,
    Button,
    Typography,
    Card, Box, TextField, InputLabel
} from '@mui/material'
import { rootLink } from '../utils/links.js'
import { Link } from 'react-router-dom'

const reducer = (state, action) => {
    switch (action.type) {
        case 'FETCHING':
            return { ...state, loading: true }
        case 'DONE_FETCHING':
            return { ...state, loading: false, data: action.payload }
        case 'ERROR':
            return {
                ...state, loading: false, error: action.payload
            }
        default:
            return { ...state, loading: false, error: null, data: null }
    }

}
const Login = () => {

    const [inputValues, setInputValues] = React.useState({
        email: '',
        password: '',
    })

    const [{ loading, error, data }, dispatch] = React.useReducer(reducer, {
        loading: false,
        error: null,
        data: null
    })


    const handleChange = (e) => {
        setInputValues(prev => {
            return {
                ...prev,
                [e.target.name]: e.target.value
            }
        })
    }

    const handleSubmit = async () => {
        try {
            dispatch({ type: 'FETCHING' })
            const formData = new FormData()
            formData.append('email', inputValues.email)
            formData.append('password', inputValues.password)
            const res = await fetch(`${rootLink}/api/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ ...inputValues })
            })

            const data = await res.json()

            dispatch({ type: 'DONE_FETCHING', payload: data.message })
            if (data.status) {
                console.log(data)
                localStorage.setItem('access_token', data.access_token)
                location.assign('/')
            }
        }
        catch (err) {
            console.log(err)
            dispatch({ type: 'ERROR', payload: err.message })
        }
    }

    return (
        <Container
            sx={{
                marginTop: '5em'
            }}
        >
            <Snackbar
                open={error || data}
                message={error || data}
                onClose={() => dispatch({ type: 'NONE' })}
            />
            <Card
                // variant=''
                elevation={7}
                sx={{
                    padding: 3,
                    display: 'flex',
                    flexDirection: 'column',
                    width: '65dvw'

                }}
            >
                <Box>

                    <Typography
                        sx={{textAlign: 'center', mb:4}}
                        variant='h5'
                    >
                        YourHR
                    </Typography>
                    <Typography
                        variant='h6'
                    >
                        Sign in
                    </Typography>
                   
                </Box>
                <Box>
                    <Box
                        className='input-container'
                    >
                        <InputLabel htmlFor='email'>Input your email :</InputLabel>
                        <TextField
                            fullWidth
                            label='email'
                            id='email'
                            value={inputValues.email}
                            onChange={(e) => handleChange(e)}
                            name='email'
                        />
                    </Box>

                    <Box
                        className='input-container'
                    >
                        <InputLabel htmlFor='password'>Input you password</InputLabel>
                        <TextField
                            fullWidth
                            type='password'
                            label='password'
                            name='password'
                            value={inputValues.password}
                            onChange={(e) => handleChange(e)}
                            id='password'
                        />
                    </Box>

                    <Box
                        className='input-container'
                    >
                        <Button
                            disabled={loading}
                            variant='contained'
                            color='info'
                            onClick={handleSubmit}
                        >

                            sign in
                        </Button>
                    </Box>


                </Box>

                <Box className='input-container'>
                    <Typography>
                        Don't have an account yet ?  <Link to='/signup'>create one!</Link>
                    </Typography>
                </Box>
            </Card>
        </Container>
    )
}

export default Login