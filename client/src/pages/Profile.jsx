import { rootLink } from '../utils/links.js'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { Card, Typography, Container, Box, Avatar, Button } from '@mui/material'
import Header from '../components/Header.jsx'

const Profile = () => {

    const navigate = useNavigate()
    const [user, setUser] = useState({})
    const [isLoggedIn, setLoggedIn] = useState(false)
    const getUser = async (token) => {
        try {

            let res = await fetch(`${rootLink}/api/auth/me`, {
                method: 'GET',
                headers: {
                    'authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            })

            const data = await res.json()
            setUser(data?.user)
            setLoggedIn(true)
        } catch (err) {
            console.log(err.message)
        }
    }

    useEffect(() => {
        if (!localStorage.getItem('access_token')) return navigate('/login')
        getUser(localStorage.getItem('access_token'))
    }, [])

    const getResume = async () => {
        try {

            const res = await fetch(`${rootLink}/api/users/resumes/${user?._id}`, {
                responseType: 'blob'
            })
            const resumeBlob = await res.blob();
            const resumeUrl = window.URL.createObjectURL(resumeBlob);
            const resumeWindow = window.open(resumeUrl);
            if (!resumeWindow || resumeWindow.closed || typeof resumeWindow.closed == 'undefined') {
                throw new Error('Failed to open resume window');
            }
        } catch (err) {
            console.log(err.message)
        }
    }
    return (
        <Container
            sx={{
                mt: 5
            }}
        >
            <Header />
            <Card
                variant='outlined'
                sx={{ p: 3 }}
            >
                <Typography
                    sx={{ p: 3, textAlign: 'center' }}
                    variant='body2'
                >
                    YourHR user profile
                </Typography>
                <Box
                    sx={{ display: 'grid', placeItems: 'center', gap: 3 }}
                >
                    <Avatar />
                    <Typography
                        variant='h4'
                    >
                        {user?.names}
                    </Typography>
                    <Typography>
                        {user?.email}
                    </Typography>

                    <Box>

                        <Button variant='contained'
                            onClick={getResume}
                        >
                            open resume
                        </Button>
                    </Box>
                </Box>
            </Card>
        </Container>
    )
}

export default Profile