import React from 'react'

const Header = () => {
    return (
        <div
            className='header'
        >
            <div>
                Y.HR
            </div>

            <div>
                <ul>
                    <button
                        onClick={() => {
                            localStorage.removeItem('access_token')
                            location.assign('/#/login')
                        }}
                    >
                        logout
                    </button>
                </ul>
            </div>
        </div>
    )
}

export default Header