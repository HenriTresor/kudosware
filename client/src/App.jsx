
import { Routes, Route } from 'react-router-dom'
import React, { lazy } from 'react'
import { useNavigate } from 'react-router-dom'
import Loading from './components/Loading'
const Login = lazy(() => import('./pages/Login'))
const Signup = lazy(() => import('./pages/Signup'))
const Profile = lazy(() => import('./pages/Profile'))
const NotFound = lazy(() => import('./pages/404'))

const App = () => {

  const navigate = useNavigate()
  React.useEffect(() => {
    if(!localStorage.getItem('access_token')) return navigate('/login') 
  },[])
  return (
    <React.Suspense fallback={<Loading />}>
      <Routes>
        <Route exact path='/login' element={<Login />} />
        <Route exact path='/signup' element={<Signup />} />
        <Route exact path='/' element={<Profile />} />
        <Route exact path='*' element={<NotFound />} />
      </Routes>
   </React.Suspense>
  )
}

export default App